import vtk

r = vtk.vtkPlot3DMetaReader()
r.SetFileName("tapered_cyl/tapered_cyl.p3d")
r.UpdateInformation()

print(r.GetOutputInformation(0))

r.UpdateTimeStep(64)
print(r.GetOutputDataObject(0).GetInformation())