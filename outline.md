* IO
	- List of file formats: VTK formats (legacy & XML), ADIOS, EnSight, Exodus, Fluent (limited support), HDF5, MFIX, NetCDF, OpenFOAM, Plot3D / Overflow, Tecplot (limited support), XDMF
	- Examples:
		- Plot3D read-plot3d.py

* Initial exploration
	- Surface view-plot3d.py
	- Slice slice-plot3d.py

* Derived quantities derived.py
	- NumPy
	- Python calculator
	- Programmable calculator

* Streamlines
	- LIC lic-plot3d.py
	- Glyph lic-plot3d.py (uncomment section)
	- Simple seeds source streamlines-plot3d.py
	- Seeding from extracts streamlines-source-plot3d.py

* Unsteady flow read-unsteady-plot3d.py, animate-plot3d.py

* Particle paths particle-path-plot3d.py, particle-animation-plot3d.py

* Streaklines streakline-plot3d.py

* Parallel
	