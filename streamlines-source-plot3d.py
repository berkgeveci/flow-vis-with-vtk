exec(open("view-plot3d.py").read())
r.AddFunction(200)

from vtk.numpy_interface import dataset_adapter as dsa
from vtk.numpy_interface import algorithms as algs
from vtk.util.vtkAlgorithm import VTKPythonAlgorithmBase
class ComputeMagnitude(VTKPythonAlgorithmBase):
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self)
        self.InputType = "vtkMultiBlockDataSet"
        self.OutputType = "vtkMultiBlockDataSet"

    def RequestData(self, request, inInfo, outInfo):
        inp = vtk.vtkDataObject.GetData(inInfo[0])
        opt = vtk.vtkDataObject.GetData(outInfo)

        opt.ShallowCopy(inp)

        inp = dsa.WrapDataObject(inp)
        opt = dsa.WrapDataObject(opt)

        m = algs.mag(inp.PointData['Velocity'])
        opt.PointData.append(m, "Velocity-magnitude")

        return 1

cv = ComputeMagnitude()
cv.SetInputConnection(r.GetOutputPort())

c = vtk.vtkContourFilter()
c.SetInputConnection(cv.GetOutputPort())
c.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_POINTS, "Velocity-magnitude")
c.SetNumberOfContours(1)
c.SetValue(0, 0.1)

m8 = vtk.vtkCompositePolyDataMapper2()
m8.SetInputConnection(c.GetOutputPort())
m8.SetScalarVisibility(0)

a8 = vtk.vtkActor()
a8.SetMapper(m8)
ren.AddActor(a8)

# sl = vtk.vtkStreamTracer()
# sl.SetInputConnection(r.GetOutputPort())
# sl.SetSourceConnection(c.GetOutputPort())
# sl.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_POINTS, "Velocity")
# sl.SetIntegratorTypeToRungeKutta45()
# sl.SetMaximumNumberOfSteps(200)
# sl.SetMaximumPropagation(10)
# #sl.SetIntegrationDirectionToBoth()

# m6 = vtk.vtkCompositePolyDataMapper2()
# m6.SetInputConnection(sl.GetOutputPort())
# m6.SetScalarVisibility(0)

# a6 = vtk.vtkActor()
# a6.SetMapper(m6)
# ren.AddActor(a6)

# prop = a6.GetProperty()
# prop.SetRenderLinesAsTubes(1)
# prop.SetLineWidth(5)

renwin.Render()

iren.Start()
