import vtk

foam = vtk.vtkOpenFOAMReader()
foam.SetFileName("damBreakWithObstacle/damBreakWithObstacle.foam")
foam.SetSkipZeroTime(1)
foam.SetCreateCellToPoint(1)
foam.SetDecomposePolyhedra(1)
foam.UpdateInformation()

tsteps = foam.GetOutputInformation(0).Get(vtk.vtkStreamingDemandDrivenPipeline.TIME_STEPS())
print(tsteps)

narrays = foam.GetNumberOfCellArrays()
for i in range(narrays):
    name = foam.GetCellArrayName(i)
    print(name, foam.GetCellArrayStatus(name))
