import vtk

r = vtk.vtkPlot3DMetaReader()
r.SetFileName("tapered_cyl/tapered_cyl.p3d")

ren = vtk.vtkRenderer()
renwin = vtk.vtkRenderWindow()
renwin.SetSize(2500, 2000)
renwin.AddRenderer(ren)
renwin.Render()

iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renwin)
iren.GetInteractorStyle().SetCurrentStyleToTrackballCamera()

eg = vtk.vtkExtractGrid()
eg.SetInputConnection(r.GetOutputPort())
eg.SetVOI(0, 63, 0, 0, 0, 31)

dss = vtk.vtkDataSetSurfaceFilter()
dss.SetInputConnection(eg.GetOutputPort())

m2 = vtk.vtkCompositePolyDataMapper2()
m2.SetInputConnection(dss.GetOutputPort())
m2.SetScalarVisibility(0)

a2 = vtk.vtkActor()
a2.SetMapper(m2)

ren.AddActor(a2)



ls = vtk.vtkLineSource()
ls.SetPoint1(-3, -4, 16)
ls.SetPoint2(-3, 4, 16)
ls.SetResolution(40)

paths = vtk.vtkParticlePathFilter()
paths.SetInputConnection(r.GetOutputPort())
paths.AddSourceConnection(ls.GetOutputPort())
paths.SetTerminationTime(100)

mapper = vtk.vtkCompositePolyDataMapper2()
mapper.SetInputConnection(paths.GetOutputPort())
mapper.SetColorModeToMapScalars()
mapper.SetScalarModeToUsePointFieldData()
mapper.SelectColorArray("Density")
mapper.SetScalarRange(0.96, 1.01)

actor = vtk.vtkActor()
actor.SetMapper(mapper)

ren.AddActor(actor)

ren.ResetCamera()
renwin.Render()

iren.Start()
