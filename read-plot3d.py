import vtk

r = vtk.vtkMultiBlockPLOT3DReader()
r.SetXYZFileName("lox.xyz")
r.SetQFileName("lox.q")
r.SetAutoDetectFormat(True)
r.Update()

print(r.GetOutput())