exec(open("read-openfoam.py").read())

def keyPressObserver(obj,event):
    global cutter, renwin, tsteps
    key = obj.GetKeySym()
    if key == 's':
        import time
        for t in tsteps:
            cutter.UpdateTimeStep(t)
            renwin.Render()
            time.sleep(0.1)
    return

ren = vtk.vtkRenderer()

renwin = vtk.vtkRenderWindow()
renwin.SetWantsBestResolution(False)
renwin.SetSize(2500, 2000)
renwin.AddRenderer(ren)
renwin.Render()

iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renwin)
iren.GetInteractorStyle().SetCurrentStyleToTrackballCamera()
iren.AddObserver(vtk.vtkCommand.KeyPressEvent, keyPressObserver)

dss = vtk.vtkDataSetSurfaceFilter()
dss.SetInputConnection(foam.GetOutputPort())

fe = vtk.vtkFeatureEdges()
fe.SetInputConnection(dss.GetOutputPort())
fe.Update()

m = vtk.vtkCompositePolyDataMapper2()
m.SetInputConnection(fe.GetOutputPort())
m.SetScalarVisibility(0)

a = vtk.vtkActor()
a.SetMapper(m)
ren.AddActor(a)

cutter = vtk.vtkCutter()
cutter.SetInputConnection(foam.GetOutputPort())

plane = vtk.vtkPlane()
plane.SetOrigin(0.5, 0.5, 0.5)
plane.SetNormal(1, 0, 0)

cutter.SetCutFunction(plane)

lic = vtk.vtkCompositeSurfaceLICMapper()
lic.SetInputConnection(cutter.GetOutputPort())
lic.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_POINTS, "U")
lic.SetColorModeToMapScalars()
lic.SetScalarModeToUsePointFieldData()
lic.SelectColorArray("alpha1")
lic.SetScalarRange(0, 1)

licactor = vtk.vtkActor()
licactor.SetMapper(lic)
ren.AddActor(licactor)

params = lic.GetLICInterface()
params.SetNumberOfSteps(10)
params.SetStepSize(2)
params.SetNoiseTextureSize(50)

ren.ResetCamera()
renwin.Render()
iren.Start()
