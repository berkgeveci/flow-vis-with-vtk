import vtk

r = vtk.vtkMultiBlockPLOT3DReader()
r.SetXYZFileName("lox.xyz")
r.SetQFileName("lox.q")
r.SetAutoDetectFormat(True)
r.Update()

print(r.GetOutput())

outline = vtk.vtkStructuredGridOutlineFilter()
outline.SetInputConnection(r.GetOutputPort())
outline.Update()

print(outline.GetOutputDataObject(0))

m = vtk.vtkCompositePolyDataMapper2()
m.SetInputConnection(outline.GetOutputPort())

a = vtk.vtkActor()
a.SetMapper(m)

ren = vtk.vtkRenderer()
ren.AddActor(a)

renwin = vtk.vtkRenderWindow()
renwin.SetWantsBestResolution(False)
renwin.SetSize(2500, 2000)
renwin.AddRenderer(ren)
renwin.Render()

iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renwin)
iren.GetInteractorStyle().SetCurrentStyleToTrackballCamera()

eg = vtk.vtkExtractGrid()
eg.SetInputConnection(r.GetOutputPort())
eg.SetVOI(0, 0, 0, 75, 0, 37)

dss = vtk.vtkDataSetSurfaceFilter()
dss.SetInputConnection(eg.GetOutputPort())

m2 = vtk.vtkCompositePolyDataMapper2()
m2.SetInputConnection(dss.GetOutputPort())

a2 = vtk.vtkActor()
a2.SetMapper(m2)

ren.AddActor(a2)

ren.Render()

eg2 = vtk.vtkExtractGrid()
eg2.SetInputConnection(r.GetOutputPort())
eg2.SetVOI(0, 37, 0, 75, 0, 0)

dss2 = vtk.vtkDataSetSurfaceFilter()
dss2.SetInputConnection(eg2.GetOutputPort())

m3 = vtk.vtkCompositePolyDataMapper2()
m3.SetInputConnection(dss2.GetOutputPort())

a3 = vtk.vtkActor()
a3.GetProperty().SetRepresentationToWireframe()
a3.SetMapper(m3)
ren.AddActor(a3)

renwin.Render()

#iren.Start()