exec(open("view-plot3d.py").read())

from vtk.numpy_interface import dataset_adapter as dsa
from vtk.util.vtkAlgorithm import VTKPythonAlgorithmBase
class ComputeVelocity(VTKPythonAlgorithmBase):
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self)
        self.InputType = "vtkMultiBlockDataSet"
        self.OutputType = "vtkMultiBlockDataSet"

    def RequestData(self, request, inInfo, outInfo):
        inp = vtk.vtkDataObject.GetData(inInfo[0])
        opt = vtk.vtkDataObject.GetData(outInfo)

        opt.ShallowCopy(inp)

        inp = dsa.WrapDataObject(inp)
        opt = dsa.WrapDataObject(opt)

        v = inp.PointData['Momentum'] / inp.PointData['Density']
        opt.PointData.append(v, "Velocity-computed")

        return 1

cv = ComputeVelocity()
cv.SetInputConnection(r.GetOutputPort())

cutter = vtk.vtkCutter()
cutter.SetInputConnection(cv.GetOutputPort())

plane = vtk.vtkPlane()
plane.SetOrigin(0, 0, 2)
plane.SetNormal(0, 0, 1)

cutter.SetCutFunction(plane)

lic = vtk.vtkCompositeSurfaceLICMapper()
lic.SetInputConnection(cutter.GetOutputPort())
lic.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_POINTS, "Velocity-computed")

licactor = vtk.vtkActor()
licactor.SetMapper(lic)
ren.AddActor(licactor)

params = lic.GetLICInterface()
params.SetNumberOfSteps(10)
params.SetStepSize(2)
params.SetNoiseTextureSize(50)

cutter2 = vtk.vtkCutter()
cutter2.SetInputConnection(r.GetOutputPort())

plane2 = vtk.vtkPlane()
plane2.SetOrigin(0, 0, 2)
plane2.SetNormal(0, 1, 0)

cutter2.SetCutFunction(plane2)

lic2 = vtk.vtkCompositeSurfaceLICMapper()
lic2.SetInputConnection(cutter2.GetOutputPort())
lic2.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_POINTS, "Velocity-computed")

licactor2 = vtk.vtkActor()
licactor2.SetMapper(lic2)
ren.AddActor(licactor2)

params = lic2.GetLICInterface()
params.SetNumberOfSteps(10)
params.SetStepSize(2)
params.SetNoiseTextureSize(50)

# glyph = vtk.vtkGlyph3D()
# glyph.SetInputConnection(cutter2.GetOutputPort())
# glyph.SetScaleFactor(0.5)

# gs = vtk.vtkGlyphSource2D()
# gs.SetGlyphTypeToArrow()
# gs.SetFilled(0)
# glyph.SetSourceConnection(gs.GetOutputPort())

# m5 = vtk.vtkCompositePolyDataMapper2()
# m5.SetInputConnection(glyph.GetOutputPort())
# m5.SetScalarVisibility(0)

# a5 = vtk.vtkActor()
# a5.SetMapper(m5)
# a5.GetProperty().SetColor(1,1,1)
# ren.AddActor(a5)

# licactor2.SetVisibility(0)

renwin.Render()

iren.Start()
