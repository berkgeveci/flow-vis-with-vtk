exec(open("view-plot3d.py").read())

# axes = vtk.vtkAxesActor()

# axes.SetTotalLength( 1.2, 1.2 , 1.2 );
# axes.SetNormalizedShaftLength( 0.85, 0.85, 0.85 );
# axes.SetNormalizedTipLength( 0.15, 0.15, 0.15 );
# axes.SetPosition(-15, -15, -20)

# property = axes.GetXAxisTipProperty();
# property.SetDiffuse(0);
# property.SetAmbient(1);
# property.SetColor( 1, 0, 1 );

# property = axes.GetYAxisTipProperty();
# property.SetDiffuse(0);
# property.SetAmbient(1);
# property.SetColor( 1, 1, 0 );

# property = axes.GetZAxisTipProperty();
# property.SetDiffuse(0);
# property.SetAmbient(1);
# property.SetColor( 0, 1, 1 );

# ren.AddActor(axes)

# print(r.GetOutputDataObject(0).GetBlock(0).GetBounds())

cutter = vtk.vtkCutter()
cutter.SetInputConnection(r.GetOutputPort())

plane = vtk.vtkPlane()
plane.SetOrigin(0, 0, 2)
plane.SetNormal(0, 0, 1)

cutter.SetCutFunction(plane)

m4 = vtk.vtkCompositePolyDataMapper2()
m4.SetInputConnection(cutter.GetOutputPort())
m4.SetColorModeToMapScalars()
m4.SetScalarModeToUsePointFieldData()
m4.SelectColorArray("StagnationEnergy")
m4.SetScalarRange(-0.541506290435791, 4.395837306976318)

a4 = vtk.vtkActor()
a4.SetMapper(m4)
ren.AddActor(a4)

cutter2 = vtk.vtkCutter()
cutter2.SetInputConnection(r.GetOutputPort())

plane2 = vtk.vtkPlane()
plane2.SetOrigin(0, 0, 2)
plane2.SetNormal(0, 1, 0)

cutter2.SetCutFunction(plane2)

m5 = vtk.vtkCompositePolyDataMapper2()
m5.SetInputConnection(cutter2.GetOutputPort())
m5.SetColorModeToMapScalars()
m5.SetScalarModeToUsePointFieldData()
m5.SelectColorArray("StagnationEnergy")
m5.SetScalarRange(-0.541506290435791, 4.395837306976318)

a5 = vtk.vtkActor()
a5.SetMapper(m5)
ren.AddActor(a5)

renwin.Render()

iren.Start()
