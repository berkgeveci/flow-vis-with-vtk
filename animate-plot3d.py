import vtk

def keyPressObserver(obj,event):
    global cutter, renwin
    key = obj.GetKeySym()
    if key == 's':
        import time
        for i in range(1, 101):
            cutter.UpdateTimeStep(i)
            renwin.Render()
            time.sleep(0.1)
    return

r = vtk.vtkPlot3DMetaReader()
r.SetFileName("tapered_cyl/tapered_cyl.p3d")

cutter = vtk.vtkCutter()
cutter.SetInputConnection(r.GetOutputPort())

plane = vtk.vtkPlane()
plane.SetOrigin(0, 0, 16)
plane.SetNormal(0, 0, 1)

cutter.SetCutFunction(plane)

mapper = vtk.vtkCompositePolyDataMapper2()
mapper.SetInputConnection(cutter.GetOutputPort())
mapper.SetColorModeToMapScalars()
mapper.SetScalarModeToUsePointFieldData()
mapper.SelectColorArray("Density")
mapper.SetScalarRange(0.96, 1.01)

actor = vtk.vtkActor()
actor.SetMapper(mapper)

ren = vtk.vtkRenderer()
ren.AddActor(actor)

renwin = vtk.vtkRenderWindow()
renwin.SetSize(2500, 2000)
renwin.AddRenderer(ren)
renwin.Render()

iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renwin)
iren.GetInteractorStyle().SetCurrentStyleToTrackballCamera()
iren.AddObserver(vtk.vtkCommand.KeyPressEvent, keyPressObserver)

iren.Start()
