exec(open("view-plot3d.py").read())
r.AddFunction(200)

sl = vtk.vtkStreamTracer()
sl.SetInputConnection(r.GetOutputPort())
sl.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_POINTS, "Velocity")
sl.SetIntegratorTypeToRungeKutta45()
sl.SetMaximumNumberOfSteps(1000)
sl.SetMaximumPropagation(100)
#sl.SetIntegrationDirectionToBoth()

ls = vtk.vtkLineSource()
ls.SetPoint1(1.8, 0, 0)
ls.SetPoint2(3.1, 0, 4.8)
ls.SetResolution(20)

sl.SetSourceConnection(ls.GetOutputPort())

m6 = vtk.vtkPolyDataMapper()
m6.SetInputConnection(sl.GetOutputPort())
m6.SetScalarVisibility(0)

a6 = vtk.vtkActor()
a6.SetMapper(m6)
a6.GetProperty().SetColor(1, 0.5, 0.5)
ren.AddActor(a6)

# prop = a6.GetProperty()
# prop.SetRenderLinesAsTubes(1)
# prop.SetLineWidth(10)

m7 = vtk.vtkPolyDataMapper()
m7.SetInputConnection(ls.GetOutputPort())

a7 = vtk.vtkActor()
a7.SetMapper(m7)
ren.AddActor(a7)

renwin.Render()

iren.Start()
