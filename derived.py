import vtk

r = vtk.vtkMultiBlockPLOT3DReader()
r.SetXYZFileName("lox.xyz")
r.SetQFileName("lox.q")
r.AddFunction(200)
r.SetAutoDetectFormat(True)
r.Update()


from vtk.numpy_interface import dataset_adapter as dsa

mesh = dsa.WrapDataObject(r.GetOutputDataObject(0))
v = mesh.PointData['Momentum'] / mesh.PointData['Density']
dff = v - mesh.PointData['Velocity']

from vtk.numpy_interface import algorithms as algs
print(algs.max(algs.abs(dff)))

############ With Python algorithm
from vtk.util.vtkAlgorithm import VTKPythonAlgorithmBase
class ComputeVelocity(VTKPythonAlgorithmBase):
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self)
        self.InputType = "vtkMultiBlockDataSet"
        self.OutputType = "vtkMultiBlockDataSet"

    def RequestData(self, request, inInfo, outInfo):
        inp = vtk.vtkDataObject.GetData(inInfo[0])
        opt = vtk.vtkDataObject.GetData(outInfo)

        opt.ShallowCopy(inp)

        inp = dsa.WrapDataObject(inp)
        opt = dsa.WrapDataObject(opt)

        v = inp.PointData['Momentum'] / inp.PointData['Density']
        opt.PointData.append(v, "Velocity-computed")

        return 1

cv = ComputeVelocity()
cv.SetInputConnection(r.GetOutputPort())
cv.Update()

############# Array calculator

calc = vtk.vtkArrayCalculator()
calc.SetInputConnection(r.GetOutputPort())
calc.SetAttributeTypeToPointData()
calc.AddVectorVariable("Momentum", "Momentum", 0)
calc.AddScalarVariable("Density", "Density", 0)
calc.SetFunction("Momentum/Density")
# The output array will be called resArray
calc.SetResultArrayName("Velocity-computed")

calc.Update()

mesh = dsa.WrapDataObject(calc.GetOutputDataObject(0))
dff = mesh.PointData['Velocity'] - mesh.PointData['Velocity-computed']
print(algs.max(algs.abs(dff)))
