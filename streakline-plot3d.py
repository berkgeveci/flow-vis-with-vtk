import vtk

r = vtk.vtkPlot3DMetaReader()
r.SetFileName("tapered_cyl/tapered_cyl.p3d")

ren = vtk.vtkRenderer()
renwin = vtk.vtkRenderWindow()
renwin.SetSize(2500, 2000)
renwin.AddRenderer(ren)
renwin.Render()

iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renwin)
iren.GetInteractorStyle().SetCurrentStyleToTrackballCamera()

eg = vtk.vtkExtractGrid()
eg.SetInputConnection(r.GetOutputPort())
eg.SetVOI(0, 63, 0, 0, 0, 31)

dss = vtk.vtkDataSetSurfaceFilter()
dss.SetInputConnection(eg.GetOutputPort())

m2 = vtk.vtkCompositePolyDataMapper2()
m2.SetInputConnection(dss.GetOutputPort())
m2.SetScalarVisibility(0)

a2 = vtk.vtkActor()
a2.SetMapper(m2)

ren.AddActor(a2)

ls = vtk.vtkLineSource()
ls.SetPoint1(-3, -4, 16)
ls.SetPoint2(-3, 4, 16)
ls.SetResolution(20)

paths = vtk.vtkStreaklineFilter()
paths.SetInputConnection(r.GetOutputPort())
paths.AddSourceConnection(ls.GetOutputPort())
paths.SetTerminationTime(100)

mapper = vtk.vtkCompositePolyDataMapper2()
mapper.SetInputConnection(paths.GetOutputPort())
mapper.SetColorModeToMapScalars()
mapper.SetScalarModeToUsePointFieldData()
mapper.SelectColorArray("Density")
mapper.SetScalarRange(0.96, 1.01)

actor = vtk.vtkActor()
actor.SetMapper(mapper)

ren.AddActor(actor)

def keyPressObserver(obj,event):
    global tracer, renwin
    key = obj.GetKeySym()
    if key == 's':
        import time
        for i in range(1, 101):
            tracer.UpdateTimeStep(i)
            renwin.Render()
            time.sleep(0.1)
    return
iren.AddObserver(vtk.vtkCommand.KeyPressEvent, keyPressObserver)

tracer = vtk.vtkParticleTracer()
tracer.SetInputConnection(r.GetOutputPort())
tracer.AddSourceConnection(ls.GetOutputPort())
tracer.SetForceReinjectionEveryNSteps(1)

mapper2 = vtk.vtkCompositePolyDataMapper2()
mapper2.SetInputConnection(tracer.GetOutputPort())
mapper2.SetScalarVisibility(0)

actor2 = vtk.vtkActor()
actor2.SetMapper(mapper2)
prop = actor2.GetProperty()
prop.SetRenderPointsAsSpheres(1)
prop.SetPointSize(12)

ren.AddActor(actor2)

ren.ResetCamera()
iren.Start()
