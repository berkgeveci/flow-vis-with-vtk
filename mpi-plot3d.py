from mpi4py import MPI
import vtk

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

contr = vtk.vtkMPIController()
contr.Initialize()
contr.SetGlobalController(contr)

r = vtk.vtkMPIMultiBlockPLOT3DReader()
r.SetXYZFileName("lox.xyz")
r.SetQFileName("lox.q")
r.SetAutoDetectFormat(True)
r.AddFunction(200)

ren = vtk.vtkRenderer()

renwin = vtk.vtkRenderWindow()
renwin.SetSize(2500, 2000)
renwin.AddRenderer(ren)
if rank > 0:
    renwin.SetOffScreenRendering(1)

eg = vtk.vtkExtractGrid()
eg.SetInputConnection(r.GetOutputPort())
eg.SetVOI(0, 0, 0, 75, 0, 37)

dss = vtk.vtkDataSetSurfaceFilter()
dss.SetInputConnection(eg.GetOutputPort())

m2 = vtk.vtkCompositePolyDataMapper2()
m2.SetInputConnection(dss.GetOutputPort())
m2.SetPiece(rank)
m2.SetNumberOfPieces(size)

a2 = vtk.vtkActor()
a2.SetMapper(m2)
ren.AddActor(a2)

sl = vtk.vtkPStreamTracer()
sl.SetInputConnection(r.GetOutputPort())
sl.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_POINTS, "Velocity")
sl.SetIntegratorTypeToRungeKutta45()
sl.SetMaximumNumberOfSteps(1000)
sl.SetMaximumPropagation(100)
sl.SetIntegrationDirectionToBoth()

ls = vtk.vtkLineSource()
ls.SetPoint1(1.8, 0, 0)
ls.SetPoint2(3.1, 0, 4.8)
ls.SetResolution(20)

sl.SetSourceConnection(ls.GetOutputPort())

pid = vtk.vtkProcessIdScalars()
pid.SetInputConnection(sl.GetOutputPort())

m6 = vtk.vtkCompositePolyDataMapper2()
m6.SetInputConnection(pid.GetOutputPort())
m6.SetColorModeToMapScalars()
m6.SetScalarModeToUsePointFieldData()
m6.SelectColorArray("ProcessId")
m6.SetScalarRange(0,4)
m6.SetPiece(rank)
m6.SetNumberOfPieces(size)

a6 = vtk.vtkActor()
a6.SetMapper(m6)
ren.AddActor(a6)

syncWindows = vtk.vtkSynchronizedRenderWindows()
syncWindows.SetRenderWindow(renwin)
syncWindows.SetParallelController(contr)
syncWindows.SetIdentifier(1)

syncRenderers = vtk.vtkCompositedSynchronizedRenderers()
syncRenderers.SetRenderer(ren)
syncRenderers.SetParallelController(contr)

if rank == 0:
    iren = vtk.vtkRenderWindowInteractor()
    iren.SetRenderWindow(renwin)
    iren.GetInteractorStyle().SetCurrentStyleToTrackballCamera()
    iren.Start()
    contr.TriggerBreakRMIs()
else:
    contr.ProcessRMIs()

contr.Finalize()
contr.SetGlobalController(None)